# javanese at gnome

## xkb

Untuk menguji :

### BlankOn XI uluwatu

Pasang font Aksara Jawa selain Javanese.

Font Aksara Jawa dapat diunduh dari:

https://aksaradinusantara.com/fonta/aksara/jawa

Salin berkas jav ke :
`/usr/share/X11/xkb/symbols`

Bisa menggunakan perintah di terminal:
`sudo cp jav /usr/share/X11/xkb/symbols`

Untuk mengurangi resiko;

* pada pengaturan pengguna, dibuat log masuk otomatis
* pengaturan privasi, kunci layar dimatikan

Untuk penggantian sumber masukan salin dua baris perintah di bawah ini pada di Terminal.

`setxkbmap -rules evdev -layout jav`

`setxkbmap -rules evdev -layout us`

selanjutnya pada jendela terminal yang sama, gunakan tanda panah ke atas dan ke bawah untuk memilih perintah

`setxkbmap -rules evdev -layout jav`

atau

`setxkbmap -rules evdev -layout us`

tekan tombol `enter`

selamat mecoba

---

## openSUSE Leap 15.1

0. Pasang font Aksara Jawa misalnya New Kramawirya.ttf
Font-font Aksara Jawa dapat diunduh dari aksarandiusantara.com
salin berkas .ttf ke

/usr/share/fonts/truetype/

1. Salin berkas jav ke :

`/usr/share/X11/xkb/symbols`

Bisa menggunakan perintah di terminal:

`sudo cp jav /usr/share/X11/xkb/symbols`


2. Buat jaga-jaga, salin dan simpan /usr/share/X11/xkb/rules/evdev.xml

3. Sesuaikan berkas /usr/share/X11/xkb/rules/evdev.xml

tambahkan kode di bawah ini:

'''
<layout>
      <configItem>
        <name>jav</name>
        <shortDescription>jav</shortDescription>
        <description>Indonesian (Javanese)</description>
        <languageList>
          <iso639Id>jav</iso639Id>
        </languageList>
      </configItem>
      <variantList/>
    </layout>
'''

di bawah

 <layout>
      <configItem>
        <name>id</name>
        <shortDescription>id</shortDescription>
        <description>Indonesian (Jawi)</description>
        <languageList>
          <iso639Id>ind</iso639Id>
          <iso639Id>msa</iso639Id>
          <iso639Id>min</iso639Id>
          <iso639Id>ace</iso639Id>
          <iso639Id>bjn</iso639Id>
          <iso639Id>tsg</iso639Id>
          <iso639Id>mfa</iso639Id>
        </languageList>
      </configItem>
      <variantList/>
    </layout>


3. Pada pengaturan Region & Language > Input Source
klik tombol tanda tambah (+), cari Indonesian > Other > Indoneian (Javanese)

4. log Out

selamat mencoba
